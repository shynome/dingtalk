require('dotenv').config()
const { Bot } = require('../')
const assert = require('assert')
const bot = new Bot(process.env.web_hook)
describe('Bot send message',()=>{

   it('text type ',async ()=>{
     let { body } = await bot.send({
       msgtype:'text',
       text:{
         content:`text type`
       }
     })
     assert.deepStrictEqual(
       body.errcode,
       0
     )
   })

  it('link type ',async ()=>{
    let { body } = await bot.send({
      msgtype:'link',
      link:{
        title: 'link type',
        messageUrl: 'https://baidu.com',
        picUrl: 'https://gogs.self.fevercomm.com/img/favicon.png',
        text: 'link type content'
      }
    })
    assert.deepStrictEqual(
      body.errcode,
      0
    )
  })

  it('markdown type ',async ()=>{
    let { body } = await bot.send({
      msgtype:'markdown',
      markdown:{
        title:'markdown type',
        text:[
          '## title',
          '* 44',
          '* 888',
        ].join('\n')
      }
    })
    assert.deepStrictEqual(
      body.errcode,
      0
    )
  })

  it('action card type single btn',async ()=>{
    let { body } = await bot.send({
      msgtype:'actionCard',
      actionCard:{
        title: 'action card type',
        text: 'action card type content ',
        singleTitle: 'baidu',
        singleURL: 'https://baidu.com'
      },
    })
    assert.deepStrictEqual(
      body.errcode,
      0
    )
  })

  it('action card type multi btns',async ()=>{
    let { body } = await bot.send({
      msgtype:'actionCard',
      actionCard:{
        title: 'action card type',
        text: 'action card type content ',
        btns:[
          { title:'gogs', actionURL: 'https://gogs.self.fevercomm.com' },
          { title:'drone', actionURL: 'https://drone.self.fevercomm.com' },
          { title:'nextcloud', actionURL: 'https://nextcloud.self.fevercomm.com' },
        ]
      },
    })
    assert.deepStrictEqual(
      body.errcode, 
      0
    )
  })

  it('feed card type',async ()=>{
    let { body } = await bot.send({
      msgtype:'feedCard',
      feedCard:{
        links:[
          { title:'gogs', messageURL: 'https://gogs.self.fevercomm.com', picURL:'https://gogs.self.fevercomm.com/img/favicon.png' },
          { title:'drone', messageURL: 'https://drone.self.fevercomm.com', picURL:'https://gogs.self.fevercomm.com/img/favicon.png' },
          { title:'nextcloud', messageURL: 'https://nextcloud.self.fevercomm.com' },
        ]
      }
    })
    assert.deepStrictEqual(
      body.errcode, 
      0
    )
  })
  
})
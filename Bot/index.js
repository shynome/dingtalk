/// <reference path="../types/dingtalk/index.d.ts" />
const got = require('got')
class Bot {
  /**@param {string} webhook 机器人 webhook 链接 */
  constructor(webhook){
    this.webhook = webhook
  }
  /**@param {DingTalk.Message} body */
  send(body){
    return got.post(this.webhook,{ json:true, body, })
  }
}
exports.Bot = Bot
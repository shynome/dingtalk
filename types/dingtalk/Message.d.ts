
export interface At {
  atMobiles?: string[]
  isAtAll?: boolean
}

export interface TextMessage {
  msgtype: 'text'
  text: {
    /**消息内容 */
    content: string
  }
  at?: At
}

export interface LinkMessage {
  msgtype: 'link'
  link:{
    /**消息标题 */
    title: string
    /**消息内容。如果太长只会部分展示 */
    text: string
    /**图片URL */
    picUrl?: string
    /**点击消息跳转的URL */
    messageUrl: string
  }
}

export interface MarkdownMessage {
  msgtype: 'markdown'
  markdown: {
    /**首屏会话透出的展示内容 */
    title: string
    /**markdown格式的消息 */
    text: string
  }
  at?: At
}

export interface ActionCardMessage {
  msgtype: 'actionCard'
  actionCard: {
    /**首屏会话透出的展示内容 */
    title: string
    /**markdown格式的消息 */
    text: string
    /**0-正常发消息者头像,1-隐藏发消息者头像 */
    hideAvatar?: '0'|'1'
    /**按钮排列方式: 0-按钮竖直排列，1-按钮横向排列 */
    btnOrientation?: '0'|'1'
    /**单个按钮的方案。(设置此项和singleURL后btns无效。) */
    singleTitle?: string
    /**点击singleTitle按钮触发的URL */
    singleURL?: string
    btns?: {
      /**按钮方案 */
      title: string
      /**点击按钮触发的URL */
      actionURL: string
    }[]
  }
}

export interface FeedCardMessage {
  msgtype: 'feedCard'
  feedCard: {
    links:{
      title: string
      messageURL: string
      picURL?: string
    }[]
  }
}

export type Message = TextMessage | LinkMessage | MarkdownMessage | ActionCardMessage | FeedCardMessage
